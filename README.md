# Apiator [![Download][artifact_img]][artifact] [![Build Status][build_img_master]][build_master] [![Build Status][build_img_develop]][build_develop]

  [artifact_img]: https://api.bintray.com/packages/ainrif/maven/apiator/images/download.svg
  [artifact]: https://bintray.com/ainrif/maven/apiator/_latestVersion
  [build_img_master]: https://semaphoreci.com/api/v1/projects/41a2aec3-a7dd-48d2-8b5a-ea01186d58b2/500313/shields_badge.svg
  [build_master]: https://semaphoreci.com/katoquro/apiator/branches/master
  [build_img_develop]: https://semaphoreci.com/api/v1/projects/41a2aec3-a7dd-48d2-8b5a-ea01186d58b2/497610/shields_badge.svg
  [build_develop]: https://semaphoreci.com/katoquro/apiator/branches/develop
  
Simple library for auto-documenting of you java/groovy rest-api

## Getting started

You can find a draft of documentation [here](http://ainrif.bitbucket.org/) or look into SmokeSpec.groovy

## How you can help

* If you find some bugs please create an issue [here](https://bitbucket.org/ainrif/apiator/issues)
* If you want to suggest feature also create an [issue](https://bitbucket.org/ainrif/apiator/issues)
or vote for [existing](https://bitbucket.org/ainrif/apiator/issues?status=new&status=open)
* Or make a pool request with your feature